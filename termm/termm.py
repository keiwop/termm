#! /usr/bin/env python3
#     termm is a gtk3 terminal
#     Copyright (C) 2016 Maxime Martin
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#     For any question, you can contact me at keiwop.dev@gmail.com

#Awesome todo list:
#TODO save and restore config at runtime
#TODO restore tabs at startup
#TODO notification is broken
#TODO contextual menu 
#TODO fix the vte path recognition
#TODO replace print calls by logging
#TODO proper exit when receiving sigint
#TODO standardize the activate functions
#TODO replace/make zoom +/- icons
#TODO add shortcut to switch between splitted tab
#TODO find how to replace properly the color in the svg
#TODO wow, much big todo, awesome list
#TODO make config window
#TODO workspace detection not compatible with wayland
#TODO quirks on dual screen were kinda fixed but I broke it again :)
#TODO fuck the scrollbars and kill them with fire
#TODO check for screen change before hiding the app

#Being worked on:
#16-04-06
#TODO work on some animations
#TODO window resizing + placement is shitty and not wayland compatible
#16-03-24
#TODO change the font => ugly when changing
#16-03-21
#TODO package app for AUR
#TODO package app for pypi

#Done:
#16-04-05
# 	add more split options
#	rework the the terminal split
#	redo the tab control
#	add scrollbar to the term
#16-04-04
# 	add config to start as hidden
#	rewrite the assets paths
# 	rework the icon-change on the checkbuttons for gnome 3.20
#	fix the pixelisation in icons when hovered
#	app is updated to gnome 3.20
#16-03-28
# 	clean code for switching between light and dark themes
#	unload all assets when switching themes
#	populate the theme submenu automagically
#	the 4 main themes are now finished
#	make a beautiful default theme
#16-03-27
# 	split get_button_image function
#16-03-25
#	clipboard usage on split terminal
#	rework the shortcuts
#16-03-24
#	font-scale
# 	copy & paste
#16-03-22
#	work on theming
#	write the popover_menu (not in glade)
#16-03-21
#	app icon is installed correctly
#16-03-20
#	open new tab same path as others
#	close tab with ^D
#Before 16-03-20
#	lot of stuff not in changelog

from . import termm_config as config
from . import window_controller
from . import tab_controller
from . import app_handler
from . import theme
from . import menu
from scss.namespace import Namespace
from scss.types import String
from scss import compiler
import signal
import glob
import sys
import re
import os
import gi
from gi.repository import Gtk, Gdk, Gio, GdkPixbuf, GLib, Vte, Keybinder, Wnck
gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
gi.require_version('Vte', '2.91')
gi.require_version('Keybinder', '3.0')
gi.require_version('Notify', '0.7')
gi.require_version('Wnck', '3.0')

class Termm(Gtk.Application):
	def __init__(self):
		Gtk.Application.__init__(
				self, application_id="com.github.keiwop.termm",
				flags=Gio.ApplicationFlags.FLAGS_NONE)
		self.app_path = os.path.dirname(os.path.realpath(__file__))

		self.tab_controller = tab_controller.TabController(self)
		self.window_controller = window_controller.WindowController(self)
		self.handler = app_handler.Handler(self)
		self.theme = theme.Theme(self)
		self.menu = menu.Menu(self)

		self.builder = self.init_builder()
		self.header_button_list = []
		self.button_list = self.get_buttons()

		self.main_window = self.get_window()
		self.revealer = self.get_revealer()
		self.header_bar = self.get_header_bar()
		self.main_stack = self.get_main_stack()
		self.scrolled_window_stack_switcher = self.get_scrolled_window_stack_switcher()
		self.stack_switcher = self.get_stack_switcher()
		self.tool_box = self.get_tool_box()
		self.shortcut_box = self.get_shortcut_box()

		self.old_stack_switcher_children = []
		self.stack_switcher_button_pressed = None
		self.is_stack_switcher_button_dragged = False
		self.drag_root_x = 0
		self.mouse_button_pressed = 0

		self.is_app_hidden = False

		self.state_focused = Gdk.WindowState.FOCUSED
		self.state_tiled = Gdk.WindowState.TILED
		self.state_maximized = Gdk.WindowState.MAXIMIZED
		self.state_fullscreen = Gdk.WindowState.FULLSCREEN

		self.wnck = Wnck.Screen.get_default()
		self.wnck.force_update()
		self.last_active_workspace = self.wnck.get_active_workspace()
		
		self.style_provider = Gtk.CssProvider()
		self.terminal_font_scale = config.terminal_font_scale
		
		self.can_show_animation = config.drop_down_animation

	def do_startup(self):
		Gtk.Application.do_startup(self)
		signal.signal(signal.SIGINT, signal.SIG_DFL)

	def do_activate(self):
		self.compile_scss()
		self.bind_hide_app_key()
		self.handler.set_tab_controller(self.tab_controller)
		self.handler.set_window_controller(self.window_controller)
		self.main_window.set_icon_from_file(config.app_icon)
		self.menu._activate()
		self.theme.set_theme(config.theme_name)
#		self.theme.update_theme_submenu()
		# self.connect_shortcuts()
		self.tab_controller.add_new_tab()
		self.main_window.show_all()
		self.window_controller.restore_state()
#		print(dir(Gdk.WindowTypeHint))
#		self.main_window.set_type_hint(Gdk.WindowTypeHint.DOCK)
#		self.main_window.set_type_hint(Gdk.WindowTypeHint.NORMAL)
		self.connect_shortcuts()
		self.add_window(self.main_window)
		self.main_window.set_opacity(config.window_opacity)
		if config.hide_on_startup:
			self.hide_app()

	# TODO Isn't working anymore, will try to find out later. Using accelerators in glade in the mean time
	# def connect_actions(self):
	# 	print("Connecting actions")
	# 	self.action_add_new_tab = Gio.SimpleAction.new_stateful("add_new_tab", None, GLib.Variant.new_boolean(False))
	# 	self.action_add_new_tab.connect("change-state", self.add_new_tab)
	# 	self.add_action(self.action_add_new_tab)
	#
#		action_test = Gio.SimpleAction(name="test")
#		action_test.connect("activate", self.test)
#		self.add_action(action_test)
	def connect_shortcuts(self):
		print("\n\nConnecting shortcuts")
		for action_name in config.shortcut_dict:
			self.connect_action(self.handler, action_name)
			self.connect_accel(action_name)

	
	def connect_action(self, module, action_name):
		action = Gio.SimpleAction.new_stateful(action_name, None, GLib.Variant.new_boolean(False))
		action.connect("change-state", getattr(module, action_name))
		self.add_action(action)
	
	def connect_accel(self, action_name):
		shortcut = config.shortcut_dict[action_name]
		self.set_accels_for_action("app."+action_name, shortcut)
		
		
#		action_next_tab = Gio.SimpleAction.new_stateful("next_tab", None, GLib.Variant.new_boolean(False))
#		action_next_tab.connect("change-state", self.test)
#		self.add_action(action_next_tab)
#		self.set_accels_for_action("app.next_tab", ["<Alt>Right"])
#		self.test()
	
	def test(self, *args):
		print("\n\n\nHELLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\n\n\n")

        
	def init_builder(self):
		builder = Gtk.Builder()
		builder.add_from_file(config.ui_main_window)
		builder.connect_signals(self.handler)
		return builder

	def get_window(self):
		main_window = self.builder.get_object("main_window")
		main_window.set_default_size(config.window_width, config.window_height)
		return main_window

	def get_revealer(self):
		return self.builder.get_object("main_revealer")

	def get_header_bar(self):
		header_bar = self.builder.get_object("header_bar")
		header_bar.set_name("header_bar")
#		self.menu.create_popover_menu()

		button_menu = self.builder.get_object("button_menu")
		button_menu.set_popover(self.menu.popover_menu)
		button_menu.set_name("header_menu")
		self.header_button_list.append(button_menu)
#		self.menu.set_button_popover_menu(button_menu)

		button_quit = self.builder.get_object("button_quit")
		button_quit.set_name("exit_app")
		self.header_button_list.append(button_quit)
		
		return header_bar

	def get_main_stack(self):
		return self.builder.get_object("main_stack")

	def get_stack_switcher(self):
		stack_switcher = self.builder.get_object("stack_switcher")
		stack_switcher.set_name("stack_switcher")
		return stack_switcher
	
	def get_scrolled_window_stack_switcher(self):
		scrolled_window_stack_switcher = self.builder.get_object("scrolled_window_stack_switcher")
		scrolled_window_stack_switcher.set_name("scrolled_window_stack_switcher")
		return scrolled_window_stack_switcher

	def get_tool_box(self):
		return self.builder.get_object("tool_box")

	def get_shortcut_box(self):
		shortcut_box = self.builder.get_object("shortcut_box")
		shortcut_box.set_name("shortcut_box")
		return shortcut_box

	def get_buttons(self):
		button_list = []
		for button_name in config.button_dict:
			if "button" in button_name:
				print("Button name:", button_name)
				button = self.builder.get_object(button_name)
				if button is not None:
					button.set_name(button_name)
					button_list.append(button)
		return button_list

	# def get_button_list(self):
	# 	return self.button_list
	# def get_button(self, button_name):
	# 	for button in self.button_list:
			# if

	def show_all(self):
		# self.active_tab.show_all()
		self.main_stack.show_all()
		self.tool_box.show_all()
		self.main_window.show()

	def bind_hide_app_key(self):
		key_bind_hide = config.hide_app_key
		Keybinder.init()
		Keybinder.bind(key_bind_hide, self.handler.on_hide_app_key_pressed)

	def toggle_hide_app(self):
		last_event_time = Keybinder.get_current_event_time()
		print("TOGGLE HIDE_APP:", last_event_time)
		print("is_active:", self.window_controller.is_active)
		print("is_focused:", self.window_controller.is_focused)

		self.wnck.force_update()
		if not self.window_controller.is_focused:
			print("App was hidden")
			self.is_app_hidden = True
			if self.last_active_workspace != self.wnck.get_active_workspace():
				self.hide_app(do_animation=False)
				self.last_active_workspace = self.wnck.get_active_workspace()
		if config.restore_on_focused_screen:
			if self.window_controller.check_focused_monitor_changed():
				self.hide_app(do_animation=False)
		
		if self.is_app_hidden is False:
			self.hide_app()
		else:
			self.show_app(last_event_time)

	def hide_app(self, do_animation=config.drop_down_animation):
		print("Hiding app")
		self.is_app_hidden = True
		self.window_controller.save_state()
		self.can_show_animation = False
		if self.window_controller.do_iconify:
			self.main_window.iconify()
		elif "drop_down" in self.window_controller.application_state and do_animation:
			self.window_controller.hide_with_animation()
			self.can_show_animation = True
		else:
			self.main_window.hide()
	
	def show_app(self, event_time=None):
		print("Showing app")
		self.is_app_hidden = False
		if "drop_down" in self.window_controller.application_state and self.can_show_animation:
			self.window_controller.show_with_animation(event_time)
		else:
			self.show_all()
#			self.window_controller.restore_state()
			if event_time is None:
				self.main_window.present()
			else:
				self.main_window.present_with_time(event_time)
		self.tab_controller.focus_active_tab()
	
	def terminate_application(self):
		print("Asked to terminate the application")
		print("Saving application state")
		self.window_controller.save_state()
		print("Exiting")
	
	def compile_scss(self):
		scss_list = glob.glob(config.scss_dir+"/*.scss")
		print("\n\n\n\nSCSS_FILES:", scss_list)
		for scss_path in scss_list:
			css_path = scss_path.replace("scss", "css")
			print("\nscss path:", scss_path)
			print("css path:", css_path)
			print("app path:", self.app_path)
			asset_path = self.app_path + "/icons/assets/checkbox-checked_"
			asset_path += os.path.basename(scss_path).split(".")[0] + ".svg"
			print("scss file:", os.path.basename(scss_path))
			print("asset_path:", asset_path)
			
			namespace = Namespace()
			namespace.set_variable("$asset_path", String(asset_path))
			css_data = compiler.compile_file(scss_path, namespace=namespace)
#			print("css:", css_data)
			with open(css_path, "w") as css_file:
				css_file.write(css_data)
			



#TODO
def replace_line_in_config(property_name, new_value):
	file_name = config.config_file
	with open(file_name, "r") as f:
		data = f.readlines()
	f.close()
	with open(file_name, "w") as f:
		for line in data:
			if is_in_line(line, property_name):
				new_line = property_name + " = " + new_value
				print("Found config: %s" % line)
				print("Replaced by: %s" % new_line)
				print(new_line, file=f, end="\n")
			else:
				print(line, file=f, end="")
	f.close()


def is_in_line(line, word):
	return re.search("^{0} =*".format(word), line)	



if __name__ == '__main__':
	app = Termm()
	app.run(sys.argv)
	app.terminate_application()

