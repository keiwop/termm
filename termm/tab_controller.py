#from .termm import Terminal
from . import termm_config as config
from . import termm_tab
import os

class TabController:
	def __init__(self, app):
		print("TAB_CONTROLLER INIT")
		self.app = app
		self.tab_list = []
		self.tab_nb = 0
		self.active_tab = None
		self.active_tab_nb = 0
		self.tab_uri_list = []


	def add_new_tab(self, tab_name=None):
		print("Adding new tab:", tab_name)
		print("tab_uri_list:", self.tab_uri_list)
		if tab_name is None:
			if len(self.tab_uri_list) > 0:
				tab_name = self.tab_uri_list[self.active_tab_nb]
			else:
				tab_name = config.terminal_default_path

		new_tab = self.create_tab(tab_name)
		self.app.old_stack_switcher_children = self.app.stack_switcher.get_children()
		self.app.main_stack.add_titled(new_tab, new_tab.get_name(), new_tab.get_name())
		self.app.theme.customize_new_stack_switcher_button()
		self.set_active_tab(new_tab)
		self.app.show_all()


	def create_tab(self, terminal_uri):
		print("Creating tab:", self.tab_nb)
		tab = termm_tab.Tab(self.app, str(len(self.tab_list)))
		tab.add_terminal(terminal_uri)
		self.tab_list.append(tab)
		self.tab_uri_list.append(terminal_uri)
		self.tab_nb = len(self.tab_list)
		return tab


	def focus_active_tab(self):
		if len(self.tab_list):
			self.active_tab.active_term.show()
			self.active_tab.active_term.grab_focus()


	def set_active_tab(self, tab):
		self.active_tab = tab
		self.active_tab.show()
		self.active_tab_nb = self.tab_list.index(self.active_tab)
		self.app.main_stack.set_visible_child(self.active_tab)
		self.focus_active_tab()


	def set_active_tab_nb(self, tab_nb):
		if tab_nb < 0:
			tab_nb = 0
		elif tab_nb >= len(self.tab_list):
			tab_nb = len(self.tab_list) - 1
		self.set_active_tab(self.tab_list[tab_nb])


	def set_active_tab_uri(self, uri):
		if self.active_tab_nb < len(self.tab_list):
			self.tab_uri_list[self.active_tab_nb] = uri
			self.app.window_controller.set_window_subtitle(uri)
			self.update_stack_switcher_buttons()


	def update_stack_switcher_buttons(self):
		for count, button in enumerate(self.app.stack_switcher.get_children()):
			if count < len(self.tab_list):
				uri = self.tab_uri_list[count]
				if uri == os.environ['HOME'] or uri == "/root":
					uri = "~"
				if uri != "/":
					uri = uri.split("/")[-1]
				button.set_label(uri)
				button.set_name("stack_switcher_button")
				if count == self.active_tab_nb:
					button.set_name("stack_switcher_button_active")


	def connect_signals_stack_switcher_button(self, button):
		print("Connecting stack switcher buttons signals:", button)
		button.connect("released", self.app.handler.on_stack_switcher_button_released)
		button.connect("motion-notify-event", self.app.handler.on_stack_switcher_button_motion_notify_event)
		button.connect("button-press-event", self.app.handler.on_stack_switcher_button_press_event)


	def split_tab(self, direction="h"):
		if "h" in direction:
			self.active_tab.hsplit(self, self.app.main_window)
		elif "v" in direction:
			self.active_tab.vsplit(self, self.app.main_window)
		self.focus_active_tab()
		self.app.show_all()


	def close_tab(self, tab_nb=None):
		print("Closing tab:", tab_nb)
		if tab_nb is None:
			tab_nb = self.active_tab_nb
		if len(self.tab_list):
			tab = self.tab_list.pop(tab_nb)
			self.tab_uri_list.pop(tab_nb)
			tab.destroy()
			if len(self.tab_list):
				if self.active_tab_nb >= len(self.tab_list):
					self.active_tab_nb = len(self.tab_list) - 1
				if tab_nb <= self.active_tab_nb:
					self.set_active_tab_nb(self.active_tab_nb - 1)
				else:
					self.set_active_tab_nb(self.active_tab_nb)
		self.update_stack_switcher_buttons()


	def close_active_tab(self):
		self.close_tab()


	def show_next_tab(self, *args):
		if self.active_tab_nb + 1 >= len(self.tab_list):
			self.set_active_tab_nb(0)
		else:
			self.set_active_tab_nb(self.active_tab_nb + 1)


	def show_prev_tab(self, *args):
		if self.active_tab_nb - 1 < 0:
			self.set_active_tab_nb(len(self.tab_list) - 1)
		else:
			self.set_active_tab_nb(self.active_tab_nb - 1)


	def move_tab(self, direction):
		tab_pos = self.get_tab_pos_from_stack_switcher_button(self.app.stack_switcher_button_pressed)
		tab = self.tab_list.pop(tab_pos)
		tab_name = self.tab_uri_list.pop(tab_pos)
		child = self.app.stack_switcher.get_children()[tab_pos]
		if "right" in direction:
			tab_pos += 1
		elif "left" in direction:
			tab_pos -= 1

		if tab_pos > len(self.tab_list):
			tab_pos = len(self.tab_list)
		elif tab_pos < 0:
			tab_pos = 0

		self.tab_list.insert(tab_pos, tab)
		self.tab_uri_list.insert(tab_pos, tab_name)
		self.app.stack_switcher.reorder_child(child, tab_pos)
		self.set_active_tab_nb(tab_pos)


	def get_tab_pos_from_stack_switcher_button(self, stack_button):
		tab_nb = 0
		for button in self.app.stack_switcher.get_children():
			if button == stack_button:
				return tab_nb
			tab_nb += 1
		return None


