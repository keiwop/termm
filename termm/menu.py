from gi.repository import Gtk
from . import termm_config as config

class Menu:
	def __init__(self, app):
		print("MENU INIT")
		self.app = app
		self.builder = None
		self.builder_theme_submenu = None
		self.button_popover_menu = None
		self.popover_menu = None
		self.button_theme_submenu = None
		self.theme_submenu = None
		self.button_fullscreen = None
		self.button_night_mode = None
#		self.button_list = []
		self.init_builders()
		self.create_popover_menu()
		self.create_theme_submenu()		
	
	def _activate(self):
		self.app.header_button_list.append(self.button_fullscreen)
		self.app.header_button_list.append(self.button_night_mode)
		self.app.header_button_list.append(self.button_zoom_plus)
		self.app.header_button_list.append(self.button_zoom_minus)
#		self.app.button_list.append(self.button_popover_menu)
	
	def set_button_popover_menu(self, button):
		self.button_popover_menu = button

	def init_builders(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(config.ui_popover_menu)
		self.builder.connect_signals(self.app.handler)
#		self.builder_theme_submenu = Gtk.Builder()
#		self.builder.add_from_file(config.ui_theme_submenu)

	def create_popover_menu(self):
#		self.builder = Gtk.Builder()
#		self.builder.add_from_file(config.ui_popover_menu)
		self.popover_menu = self.builder.get_object("popover_menu")
		self.popover_menu.set_name("popover_menu")
		
		self.button_fullscreen = self.builder.get_object("button_fullscreen")
		self.button_fullscreen.set_name("button_fullscreen")
#		self.button_list.append(self.button_fullscreen)
		
		self.button_night_mode = self.builder.get_object("button_night_mode")
		self.button_night_mode.set_name("button_night_mode")
		
		self.button_zoom_plus = self.builder.get_object("button_zoom_plus")
		self.button_zoom_plus.set_name("button_zoom_plus")
		
		self.button_zoom_minus = self.builder.get_object("button_zoom_minus")
		self.button_zoom_minus.set_name("button_zoom_minus")
#		self.button_list.append(self.button_night_mode)

	def create_theme_submenu(self):
#		self.builder_theme_submenu = Gtk.Builder()
#		self.builder_theme_submenu.add_from_file(config.file_ui_theme_submenu)
#			
		self.theme_submenu = self.builder.get_object("theme_submenu")
		self.theme_submenu.set_name("theme_submenu")

		self.button_theme_submenu = self.builder.get_object("button_theme_submenu")
		self.button_theme_submenu.set_popover(self.theme_submenu)
		
		print("THEME_SUBMENU==================================================================")
#		print(self.theme_submenu.get_children()[0].get_children()[0].get_properties(""))
		
#		for button in config.theme_list:
	
	

#		if config.override_menu_relief:
#			self.button_fullscreen.set_relief(theme.relief_button_main_menu)
#			self.button_night_mode.set_relief(theme.relief_button_main_menu)
