from . import termm_config as config
from . import terminal
from gi.repository import Gtk

class Tab(Gtk.VPaned):
	def __init__(self, app, tab_name):
		Gtk.Paned.__init__(self)
		self.set_name("tab_vpane")
		self.app = app
		self.name = tab_name
		self.active_term = None
		self.is_hsplit = False
		self.is_vsplit = False
		self.term_dict = {}
		self.activate()


	def activate(self):
		pane1 = Gtk.HPaned()
		pane1.set_vexpand(True)
		pane1.set_hexpand(True)
		self.add1(pane1)


	def get_name(self):
		return self.name	

	
	def add_terminal(self, uri):
		term = self.create_term(uri)
		self.get_child1().add1(term)
		self.term_dict[0] = term.get_children()[0]


	def get_terminal_list(self):
		terminal_list = []
		term_window_list = []
		if self.get_child1() is not None:
			term_window_list.extend(self.get_child1().get_children())
		if self.get_child2() is not None:
			term_window_list.extend(self.get_child2().get_children())
		for term_window in term_window_list:
			terminal_list.append(term_window.get_children()[0])
		return terminal_list


	def create_term(self, uri):
		scrolled_window = Gtk.ScrolledWindow()
		term = terminal.Terminal(uri)
		self.app.theme.set_terminal_theme(term)
		term.connect("notify", self.app.handler.on_vte_notify)
		term.connect("child-exited", self.app.handler.on_vte_child_exited)
		term.connect("focus-in-event", self.app.handler.on_terminal_focus_in_event)
		scrolled_window.add(term)
		scrolled_window.set_vexpand(True)
		scrolled_window.set_hexpand(True)
		self.set_active_term(term)
		return scrolled_window


	def set_active_term(self, term):
		print("Setting active terminal:", term)
		term.show()
		term.grab_focus()
		self.active_term = term


	def set_active_term_nb(self, term_nb):
		print("Setting active terminal nb:", term_nb)
		term = self.term_dict[term_nb]
		self.set_active_term(term)


	def hsplit(self, tab_controller, window):
		print("Splitting tab horizontally")
		pane1 = self.get_child1()
		pane2 = self.get_child2()
		if self.is_hsplit:
			if pane1 is not None:
				if pane1.get_child2() is not None:
					pane1.get_child2().destroy()
			if pane2 is not None:
				if pane2.get_child2() is not None:
					pane2.get_child2().destroy()
			self.set_active_term_nb(0)
			self.is_hsplit = False
		else:
			if pane1 is not None:
				term1 = self.create_term(config.terminal_default_path)
				pane1.add2(term1)
				self.term_dict[1] = term1
				self.set_handle_hposition(window, pane1)
			if pane2 is not None:
				term2 = self.create_term(config.terminal_default_path)
				pane2.add2(term2)
				self.term_dict[3] = term2
				self.set_handle_hposition(window, pane2)
			self.is_hsplit = True


	def vsplit(self, tab_controller, window):
		print("Splitting tab horizontally")
		if self.is_vsplit:
			pane2 = self.get_child2()
			if pane2 is not None:
				pane2.destroy()
			self.set_active_term_nb(0)
			self.is_vsplit = False
		else:
			pane2 = Gtk.HPaned()
			pane2.set_vexpand(True)
			pane2.set_hexpand(True)
			if pane2 is not None:
				term1 = self.create_term(config.terminal_default_path)
				pane2.add1(term1)
				self.term_dict[2] = term1
				if self.get_child1().get_child2() is not None:
					term2 = self.create_term(config.terminal_default_path)
					pane2.add2(term2)
					self.term_dict[3] = term2
					self.set_handle_hposition(window, pane2)
			self.add2(pane2)
			self.set_handle_vposition(window, self)
			self.is_vsplit = True
	
	
	def set_handle_hposition(self, window, pane):
		handle_position = window.get_size()[0] / 2
		pane.set_position(handle_position)
	
	
	def set_handle_vposition(self, window, pane):
		handle_position = window.get_size()[1] / 2
		pane.set_position(handle_position)


