from . import termm_config as config
from gi.repository import Gtk, Gdk, GLib, Gio, GdkPixbuf
import os
import re
import io

class Theme:
	def __init__(self, app):
		self.app = app
		# print("button_list:", app.button_list)
		self.is_night_mode = config.is_night_mode
		self.theme_name = config.theme_name
		# self.button_list = None

	# def set_button_list(self, button_list):
	# 	self.button_list = button_list

	def set_theme_mode(self, mode=None):
		if mode is not None:
			if "light" in mode or "day" in mode:
				self.is_night_mode = False
			elif "dark" in mode or "night" in mode:
				self.is_night_mode = True
		state = GLib.Variant.new_boolean(self.is_night_mode)
		Gtk.Settings.get_default().set_property("gtk-application-prefer-dark-theme", state)
		self.app.tab_controller.focus_active_tab()
#		self.app.tab_controller.update_stack_switcher_buttons()

	def toggle_night_mode(self):
		if self.theme_name == "light":
			self.set_theme("dark")
		elif self.theme_name == "dark":
			self.set_theme("light")
		elif self.theme_name == "solarized_light":
			self.set_theme("solarized_dark")
		elif self.theme_name == "solarized_dark":
			self.set_theme("solarized_light")
		else:
			self.is_night_mode = not self.is_night_mode
			self.set_theme_mode()

	def set_theme(self, theme_name=None):
		print("Changing to theme:", theme_name)
		if theme_name is None:
			theme_name = config.theme_name
		self.theme_name = theme_name.lower().replace(" ", "_")
		print("Formatted theme name:", self.theme_name)
		path_theme = os.path.join(config.css_dir, self.theme_name+".css")
		if os.path.isfile(path_theme):
			self.load_css(path_theme)

		self.customize_shortcut_buttons()
		self.update_theme_submenu()
		self.update_terminal_theme()
		self.set_theme_mode()
		self.set_icons(self.theme_name)

	def load_css(self, css_file):
		print("Loading CSS:", css_file)
		css = open((css_file), 'rb')
		css_data = css.read()
		css.close()
		self.app.style_provider.load_from_data(css_data)
		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(), self.app.style_provider,     
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		)	
	
	def set_icons(self, theme_name):
		print("ICON DIR EXISTS:", os.path.isdir(config.icons_dir +"/"+ theme_name))
		print("BUTTON_LIST:", self.app.button_list)
		for button in self.app.button_list:
#			print("BUTTON:", button)
			self.replace_button_image(button, button.get_name())
			button_width = config.stack_switcher_button_min_width
			button_height = config.stack_switcher_button_min_height
			button.set_size_request(button_width, button_height)
			button.set_hexpand(False)
			button.set_property("can_focus", False)
			button.show_all()
		for button in self.app.header_button_list:
			self.replace_button_image(button, button.get_name())
			button.set_property("can_focus", False)
			button.show_all()
		
			

	def replace_button_image(self, button, image_name):
		image = self.get_button_image(image_name)
		button.get_child().destroy()
		button.add(image)

	def get_button_image(self, button_name):
		image_name = config.button_dict[button_name]
		image = self.get_image_from_name(image_name)
		return image

#	If a theme is defined, I get the icon from icons/buttons/*.svg
#	Then I change the color of the svg with a regex and load it in memory
#	Finally I can create a pixbuf from this svg in memory
#	If a part of this process fail, I get the image from symbolic (system images)
	def get_image_from_name(self, image_name):
		image_path = config.icons_dir +"/buttons/"+ image_name + ".svg"
		image = None
		if os.path.isfile(image_path):
			try:
				if self.theme_name in config.terminal_themes:
					print("Getting image from path:", image_path)
					new_image = self.set_svg_color(image_path, config.terminal_themes[self.theme_name]["icons_color"])
					pixbuf = GdkPixbuf.Pixbuf.new_from_stream_at_scale(new_image, 20, 20, True)
					image = Gtk.Image.new_from_pixbuf(pixbuf)
				else:
					image = self.get_image_from_symbolic(image_name)
			except Exception:
				print("Error opening file:", image_path)
				image = self.get_image_from_symbolic(image_name)
		else:
			image = self.get_image_from_symbolic(image_name)
		return image
	
	def get_image_from_symbolic(self, image_name):
		print("Getting symbolic image:", image_name)
		icon = Gio.ThemedIcon(name=image_name)
		image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
		return image

	def set_svg_color(self, svg_path, new_color):
		svg_str = None
		with open(svg_path, "r") as svg_file:
			svg_str = svg_file.read()
#		regex_svg = re.compile(r".*style=.*fill:#\b[0-9]+\b;.*", re.IGNORECASE)
		regex_svg = re.compile("fill:#[a-fA-F0-9]+;", re.MULTILINE)
		new_svg_str = regex_svg.sub("fill:"+ new_color +";", svg_str)
		data = str.encode(new_svg_str)
		new_svg_file = Gio.MemoryInputStream.new_from_data(data, None)
		return new_svg_file
	
	def customize_new_stack_switcher_button(self):
		print("Customizing new stack switcher button")
		for button in self.app.stack_switcher.get_children():
			if button not in self.app.old_stack_switcher_children:
#				button_width = config.stack_switcher_button_min_width
#				button_height = config.stack_switcher_button_min_height
#				button.set_size_request(button_width, button_height)
				button.set_hexpand(config.stack_switcher_button_hexpand)
				self.app.tab_controller.connect_signals_stack_switcher_button(button)

	def customize_shortcut_buttons(self):
		print("Customizing shortcut buttons")
		for button in self.app.shortcut_box.get_children():
#			button_width = config.shortcut_box_button_min_width
#			button_height = config.shortcut_box_button_min_height
#			button.set_size_request(button_width, button_height)
			button.set_hexpand(config.shortcut_box_button_hexpand)

	
	def update_theme_submenu(self):
		print("theme_submenu:", self.app.menu.theme_submenu)
		for button in self.app.menu.theme_submenu.get_children()[0].get_children():
			button.handler_block_by_func(self.app.handler.on_button_theme_clicked)
			if button.get_label().lower().replace(" ", "_") == self.theme_name:
				print("Button theme submenu activated:", button)
				button.set_active(True)
				button.set_name("checkbutton_theme_submenu_active")
			else:
				button.set_active(False)
				button.set_name("checkbutton_theme_submenu")
			print("")
			button.handler_unblock_by_func(self.app.handler.on_button_theme_clicked)


	def hex_to_RGBA(self, hex_color):
		if type(hex_color) is Gdk.RGBA:
			return hex_color
		RGBA_color = Gdk.RGBA()
		RGBA_color.parse(hex_color)
		RGBA_color.to_string()
		return RGBA_color


	def update_terminal_theme(self, theme_name=None):
		print("\nUpdating terminals theme")
		if theme_name is None:
			theme_name = self.theme_name
		for tab in self.app.tab_controller.tab_list:
			terminal_list = tab.get_terminal_list()
			for terminal in terminal_list:
				self.set_terminal_theme(terminal, theme_name=theme_name)


	def set_terminal_theme(self, terminal, theme_name=None):
		if theme_name is None:
			theme_name = self.theme_name
		if theme_name in config.terminal_themes:
			terminal_fg = self.hex_to_RGBA(config.terminal_themes[theme_name]["terminal_fg"])
			terminal_bg = self.hex_to_RGBA(config.terminal_themes[theme_name]["terminal_bg"])
			palette = config.terminal_themes[theme_name]["palette"]
			for i, color in enumerate(palette):
				palette[i] = self.hex_to_RGBA(color)
			terminal.set_colors(terminal_fg, terminal_bg, palette)
		else:
			terminal.set_default_colors()			
		terminal.show_all()
#		terminal.set_color_cursor(theme.terminal_cursor)
#		terminal.set_color_cursor_foreground(theme.terminal_cursor)





