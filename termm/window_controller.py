from . import termm_config as config
from gi.repository import Gtk, Gdk, Notify
import time

class WindowController:
	def __init__(self, app):
		print("WINDOW_CONTROLLER INIT")
		self.app = app
		self.monitors = None
		self.monitor_nb = 0
		self.focused_monitor = 0
		self.window_size = (config.window_width, config.window_height)
		self.current_state = None
		self.window_position = (config.window_x, config.window_y)
		self.application_state = config.application_state
		self.do_iconify = config.iconify_window
		self.is_active = False
		self.is_focused = False
		self.is_fullscreen = False


	def set_monitors(self):
		self.monitors = self.get_monitors()


	def get_monitors(self):
		screen = self.app.main_window.get_screen()
		monitors = []
		for nb_monitor in range(screen.get_n_monitors()):
			monitors.append(screen.get_monitor_geometry(nb_monitor))
			print("Monitor", monitors[nb_monitor])
		return monitors


	def get_focused_monitor_nb(self):
		mouse_x = self.app.main_window.get_screen().get_root_window().get_pointer()[1]
		total_width = 0
		for monitor_nb, monitor in enumerate(self.monitors):
			total_width += monitor.width
			if mouse_x < total_width:
				return monitor_nb
		return 0
	
	def check_focused_monitor_changed(self):
		has_changed = False
		if self.get_focused_monitor_nb() is not self.focused_monitor:
			has_changed = True
			self.focused_monitor = self.get_focused_monitor_nb()
			self.set_geometry(self.get_drop_down_geometry(self.focused_monitor))
			self.set_position(self.get_drop_down_position(self.focused_monitor))
		return has_changed


	def set_application_state(self, new_state):
		print("setting application state:", new_state)
		if new_state in config.window_states:
			self.application_state = new_state
#			self.window_size = (config.window_width, config.window_height)
			if config.follow_mouse:
				self.app.main_window.set_property("window-position", 2)
			else:
				self.app.main_window.set_property("window-position", 0)
			
			self.app.main_window.set_type_hint(Gdk.WindowTypeHint.NORMAL)
			if "drop_down" in new_state:
				self.current_state = new_state
				self.do_iconify = config.iconify_drop
				self.app.header_bar.hide()
#				The app won't position correctly if I don't hide it after the 
#				header_bar has been hidden. Seems to be a race condition
				self.app.main_window.hide()
				self.set_geometry(self.get_drop_down_geometry(self.focused_monitor))
				self.set_position(self.get_drop_down_position(self.focused_monitor))
			else:
				if self.current_state is not new_state:
					self.current_state = new_state
					self.window_size = (config.window_width, config.window_height)
				self.do_iconify = config.iconify_window
				self.app.header_bar.show()
				self.set_geometry(self.window_size)
				self.set_position(self.window_position)


	def set_geometry(self, geometry):
		print("\n\nSetting geometry:", geometry)
		self.app.main_window.resize(geometry[0], geometry[1])
		self.app.main_window.show()
		self.window_size = geometry


	def save_geometry(self, geometry=None):
		if geometry is None:
			self.window_size = self.app.main_window.get_size()
		else:
			self.window_size = geometry
		print("Saving geometry:", self.window_size)


	def set_position(self, position):
		print("\n\nSetting window position:", position)
		self.app.main_window.move(position[0], position[1])
		self.app.main_window.show()
		self.window_position = position


	def save_position(self, position=None):
		if position is None:
			self.window_position = self.app.main_window.get_position()
		else:
			self.window_position = position
		print("Saving position:", self.window_position)


	def get_drop_down_geometry(self, monitor_nb):
		window_width = self.monitors[monitor_nb].width * config.coef_drop_width
		window_height = self.window_size[1]
#		window_height = 480
		print("WINDOW GEOMETRY:", (window_width, window_height))
		return (window_width, window_height)


	def get_drop_down_position(self, monitor_nb):
		# Tested only with 2 monitors
		window_x = config.margin_x
		window_y = config.margin_y
		for i in range(monitor_nb + 1):
			print("i:", i)
			if i >= 1:
				window_x += self.monitors[i - 1].width
		return (window_x, window_y, monitor_nb)


#	TODO rewrite
	def restore_state(self):
		self.set_window_title(config.app_title)
		self.set_window_subtitle(config.app_subtitle)
#		self.window_size = (config.window_width, config.window_height)
#		self.customize_shortcut_buttons()
		self.set_monitors()
		if config.restore_on_focused_screen:
			self.focused_monitor = self.get_focused_monitor_nb()
		self.set_application_state(self.application_state)
#		self.set_position(self.get_drop_down_position(self.focused_monitor))


	def save_state(self):
		# TODO Save monitor on which the window is showed
		# TODO Save using properties instead of getters
		self.save_geometry()
		self.save_position()


	def change_window_mode(self):
		state_index = config.window_states.index(self.application_state)
		state_index += 1
		if state_index >= len(config.window_states):
			state_index = 0
		self.set_application_state(config.window_states[state_index])


	def toggle_fullscreen(self):
		if self.is_fullscreen:
			self.is_fullscreen = False
			self.app.main_window.unfullscreen()
		else:
			self.is_fullscreen = True
			self.app.main_window.fullscreen()
			self.display_notification("App is in fullscreen, press F11 to quit", "view-fullscreen-symbolic")


	def display_notification(self, str_notif, icon_name):
		Notify.init(config.title_application)
		notification = Notify.Notification.new(config.title_application, str_notif, icon_name)
		notification.show()

	
	def set_window_title(self, title):
		self.app.header_bar.set_title(title)
	
	
	def set_window_subtitle(self, subtitle):
		self.app.header_bar.set_subtitle(subtitle)

	
#	The window manager control the hide, show, iconify and deiconify animations.
#	So I've used this technique for hiding and showing the app when in 
#	drop-down mode.
#	At first I was doing it by resizing the window, but it was awfully ugly.
#	After a bit of time, I found out that it's possible to move the window
#	over the top of the screen if it is in DOCK mode. Actually, it can also be 
#	in DESKTOP mode, but the app will then be behind the other windows
	def hide_with_animation(self):
		print("HIDING ANIMATION")
		
#		old_window_position = self.window_position
#		The +36 is here because the app is now in DOCK type so the window_size 
#		isn't exactly the real one. Might be the size of the top bar of gnome.
		time_before = time.time()
		self.move_animation("up")
		time_after = time.time()
		#print("TIME BEFOR:", time_before)
		#print("TIME AFTER:", time_after)
		#print("DURATION:", time_after - time_before)
#		self.app.main_window.set_type_hint(Gdk.WindowTypeHint.NORMAL)
		self.app.main_window.hide()
#		self.set_position(old_window_position)
#		self.app.main_window.hide()

	def show_with_animation(self, event_time=None):
		print("SHOWING ANIMATION")
#		self.app.main_window.set_type_hint(Gdk.WindowTypeHint.DOCK)
		
		
		
		self.move_animation("down")
		
#		self.app.main_window.set_type_hint(Gdk.WindowTypeHint.NORMAL)
		self.app.show_all()
		if event_time is None:
			self.app.main_window.present()
		else:
			self.app.main_window.present_with_time(event_time)
#		self.set_position((self.window_position[0], 0))
		self.set_position(self.get_drop_down_position(self.focused_monitor))

	
	def move_animation(self, direction):
		start_time = time.time()
		current_time = start_time
		elapsed_time = 0
		time_end = current_time + config.max_animation_time
		self.app.main_window.set_type_hint(Gdk.WindowTypeHint.DOCK)
		
		if config.restore_on_focused_screen:
			self.focused_monitor = self.get_focused_monitor_nb()
		window_geometry = self.get_drop_down_geometry(self.focused_monitor)
		window_position = self.get_drop_down_position(self.focused_monitor)
		
		if "up" in direction:
			self.set_geometry(window_geometry)
			self.set_position(window_position)
		elif "down" in direction:
			self.set_geometry(window_geometry)
			self.set_position((window_position[0], window_position[1] - window_geometry[1]))
			self.app.tab_controller.focus_active_tab()
			self.wait(config.max_animation_time / 4)
		
		i = 1
		x_orig = self.window_position[0]
		y_orig = self.window_position[1]
		while i*config.animation_step <= self.window_size[1] + 36 and elapsed_time <= config.max_animation_time:
			time_before_pos = time.time()
			if "up" in direction:
				self.set_position((x_orig, y_orig - i*config.animation_step))
			elif "down" in direction:
				self.set_position((x_orig, y_orig + i*config.animation_step))
#			self.app.main_window.queue_draw()
			time_after_pos = time.time()
			position_duration = time_after_pos - time_before_pos
			elapsed_time = time_after_pos - start_time
			time_left = config.max_animation_time - elapsed_time
			animation_step_left = ((self.window_size[1] + 36) / config.animation_step) - i
			if animation_step_left <= 0:
				animation_step_left = 1
			wait_time = abs((time_left / animation_step_left))
			#print("WAIT TIME:", wait_time)
			if position_duration < wait_time:
				wait_time -= position_duration
			#print("POS  TIME:", position_duration)
			#print("WAITTIME2:", wait_time)
			#print("ELAPSTIME:", elapsed_time)
			#print("TIME LEFT:", time_left)
			#print("WINDOWSIZ:", self.window_size[1] + 36)
			#print("ANIM LEFT:", animation_step_left)
			time_before_wait = time.time()
			self.wait(wait_time)
			time_after_wait = time.time()
			wait_duration = time_after_wait - time_before_wait
			#print("i:", i)
			if wait_duration < wait_time * 2:
				i += 1
			else:
				i += abs(wait_duration / wait_time)
			#print("i:", i)
			#print("WAITTIME3:", time_after_wait - time_before_wait)
			#print("LOOP TIME:", time.time() - time_before_pos)
		self.app.main_window.set_type_hint(Gdk.WindowTypeHint.NORMAL)
	


	def wait(self, wait_duration):
		current_time = time.time()
		time_end = current_time + wait_duration
		while time_end > current_time:
			current_time = time.time()
			while Gtk.events_pending():
				Gtk.main_iteration()
			Gdk.flush()


