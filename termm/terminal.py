from . import termm_config as config
from gi.repository import GLib, Vte

class Terminal(Vte.Terminal):
	def __init__(self, path_open_terminal):
		Vte.Terminal.__init__(self)
		self.set_name("terminal")
		self.pid = self.spawn_sync(
				Vte.PtyFlags.DEFAULT,
				path_open_terminal,
				[config.terminal_shell],
				[],
				GLib.SpawnFlags.DO_NOT_REAP_CHILD,
				None,
				None,
		)
		self.set_font_scale(config.terminal_font_scale)

		self.set_encoding(config.terminal_encoding)
		self.set_scrollback_lines(config.terminal_scrollback)
		self.set_scroll_on_output(config.terminal_scroll_output)
		self.set_scroll_on_keystroke(config.terminal_scroll_key)
		self.set_mouse_autohide(config.terminal_mouse_autohide)

#		self.set_colors(theme.terminal_fg, theme.terminal_bg, theme.palette)
#		self.set_color_cursor(theme.terminal_cursor)
#		self.set_color_cursor_foreground(theme.terminal_cursor)
		self.set_cursor_shape(config.terminal_cursor_shape)
	#
	#
	# if theme.terminal_bg_image != "":
	# 	self.set_background_image_file(theme.terminal_bg_image)
