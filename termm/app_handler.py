from . import termm_config as config
from gi.repository import Gdk, Pango
import re
import os

class Handler:
	def __init__(self, app):
		print("APP_HANDLER INIT")
		self.app = app
		self.tab_controller = None
		self.window_controller = None
		self.count_touchpad_scroll = 0
		self.last_window_title = None

	def set_tab_controller(self, tab_controller):
		self.tab_controller = tab_controller

	def set_window_controller(self, window_controller):
		self.window_controller = window_controller

	def on_button_new_tab_clicked(self, widget, *args):
		print("Button new tab clicked:", widget)
		self.tab_controller.add_new_tab()

	def on_button_close_tab_clicked(self, widget, *args):
		print("Button close tab clicked:", widget)
		self.tab_controller.close_tab()

	def on_button_hsplit_tab_clicked(self, widget, *args):
		print("Button hsplit tab clicked:", widget)
		self.tab_controller.split_tab(direction="h")

	def on_button_vsplit_tab_clicked(self, widget, *args):
		print("Button vsplit tab clicked:", widget)
		self.tab_controller.split_tab(direction="v")

	def on_button_next_tab_clicked(self, widget, *args):
		print("Button next tab clicked:", widget)
		self.tab_controller.show_next_tab()

	def on_button_prev_tab_clicked(self, widget, *args):
		print("Button prev tab clicked:", widget)
		self.tab_controller.show_prev_tab()

	def on_button_toggle_mode_clicked(self, widget, *args):
		print("Button toggle mode clicked:", widget)
		self.window_controller.change_window_mode()

	def on_button_fullscreen_clicked(self, widget, *args):
		print("Button fullscreen clicked:", widget)
		self.window_controller.toggle_fullscreen()

	def on_button_night_mode_clicked(self, widget):
		print("Button toggle night mode clicked:", widget)
		self.app.theme.toggle_night_mode()
	
	def on_button_quit_clicked(self, widget):
		print("Button quit clicked:", widget)
		self.app.quit()

	def on_main_stack_set_focus_child(self, stack, child):
		print("Set the focus to child:", child)
		if child is not None:
			self.tab_controller.active_tab = child
			self.tab_controller.active_tab_nb = self.tab_controller.tab_list.index(child)
			self.tab_controller.update_stack_switcher_buttons()

	def on_main_window_check_resize(self, app_window):
		if app_window.get_size() != self.window_controller.window_size:
			print("Window is resizing: ", app_window.get_size())
#			print(self.app.main_window.get_property("visible"))
#			self.window_controller.window_size = app_window.get_size()
#			if self.tab_controller.active_tab.is_split:
#				self.tab_controller.active_tab.set_handle_position(app_window)

	def on_stack_switcher_button_press_event(self, button, event):
		mouse_button = event.get_button()[1]
		self.app.mouse_button_pressed = mouse_button
		if mouse_button == 1:
			self.on_stack_switcher_button_left_click_event(event, button)
		elif mouse_button == 2:
			self.on_stack_switcher_button_middle_click_event(event, button)
		elif mouse_button == 3:
			self.on_stack_switcher_button_right_click_event(event, button)

	def on_stack_switcher_button_left_click_event(self, event, button):
		print("Left click on:", button)
		self.app.stack_switcher_button_pressed = button

	def on_stack_switcher_button_middle_click_event(self, event, button):
		print("Middle click on:", button)
		tab_nb = self.tab_controller.get_tab_pos_from_stack_switcher_button(button)
		self.tab_controller.close_tab(tab_nb=tab_nb)

	def on_stack_switcher_button_right_click_event(self, event, button):
		print("Right click on:", button)

	def on_stack_switcher_button_released(self, button):
		print("Left click released:", button)
		self.app.stack_switcher_button_pressed = None
		self.app.is_stack_switcher_button_dragged = False

	def on_stack_switcher_button_motion_notify_event(self, button, event):
		if self.app.mouse_button_pressed == 1:
			if not self.app.is_stack_switcher_button_dragged:
				self.app.drag_root_x = event.get_coords()[0]
				self.app.is_stack_switcher_button_dragged = True

			tab_size = self.app.stack_switcher.get_children()[0].get_allocation().width
			if event.get_coords()[0] >= self.app.drag_root_x + (tab_size*config.stack_switcher_button_drag_coef):
				self.tab_controller.move_tab("right")
			elif event.get_coords()[0] <= self.app.drag_root_x - (tab_size*config.stack_switcher_button_drag_coef):
				self.tab_controller.move_tab("left")

	def on_tool_box_scroll_event(self, box, event):
		print("scroll box:", event.get_scroll_deltas())
		scroll_deltas = event.get_scroll_deltas()[2]
		print("scroll_deltas: ", scroll_deltas)
		print("abs:", abs(scroll_deltas))
		if abs(scroll_deltas) > 0:
			self.count_touchpad_scroll += 1
		if scroll_deltas > 0 and self.count_touchpad_scroll > config.count_touchpad_scroll:
			self.tab_controller.show_next_tab()
			self.count_touchpad_scroll = 0
		elif scroll_deltas < 0 and self.count_touchpad_scroll > config.count_touchpad_scroll:
			self.tab_controller.show_prev_tab()
			self.count_touchpad_scroll = 0

	def on_main_window_window_state_event(self, app_window, event):
		print("new window state:", event.new_window_state)
		# print("changed mask:", event.changed_mask)
		if event.changed_mask & self.app.state_focused:
			if event.new_window_state & self.app.state_focused:
				print("IS_FOCUSED")
				self.window_controller.is_focused = True
			else:
				print("NOT_FOCUSED")
				self.window_controller.is_focused = False
				if self.app.is_app_hidden:
					print("APP WAS HIDDEN")
					self.app.can_show_animation = True
				else:
					print("APP WAS NOT HIDDEN")
					self.app.can_show_animation = False
#				self.window_controller.save_state()

	def on_main_window_notify(self, app_window, property):
		print("notify:", property)
		if "is-active" in property.name:
			print("is-active:", self.app.main_window.get_property("is-active"))
			self.window_controller.is_active = self.app.main_window.get_property("is-active")
		if "has-toplevel-focus" in property.name:
			if not self.window_controller.is_active and self.window_controller.is_focused:
				print("has-toplevel-focus:", self.app.main_window.get_property("has-toplevel-focus"))
				self.window_controller.is_focused = not self.app.main_window.get_property("has-toplevel-focus")
			
#	def on_main_window_configure_event(self, app_window, event):
#		pass
#		print("configure-event:", event.width, event.height, event.x, event.y)
#		geometry = (event.width, event.height)
#		position = (event.x, event.y)
#		self.window_controller.save_geometry(geometry=geometry)
#		self.window_controller.save_position(position=position)
#		print(dir(event))

	def on_hide_app_key_pressed(self, *args):
		print("Hide app key pressed", *args)
		self.app.toggle_hide_app()

	def on_main_window_key_press_event(self, app_window, event):
		pass
#		print("Key pressed:", event.keyval)
#		print("Value:", Gdk.keyval_name(event.keyval))

	def on_vte_notify(self, terminal, property):
#		print("Terminal:", terminal)
#		print("Property:", property)
#		print("pty:", terminal.get_property("pty"))
#		TODO Dangerous
#		pty = terminal.get_property("pty")
#		if terminal.get_property("pty") is None:
#			self.tab_controller.close_active_tab()
#		print("window-title:", terminal.get_property("window-title"))
#		print("icon-title:", terminal.get_property("icon-title"))
#		print("current-dir:", terminal.get_property("current-directory-uri"))
#		print("current-dir2:", terminal.get_current_directory_uri())
		vte_window_title = terminal.get_property("window-title")
#		TODO test if vte_window_title != last_window_title breaks something
		if vte_window_title != self.last_window_title:
			self.last_window_title = vte_window_title
			if vte_window_title is not None:
				curr_dir_regex = re.compile("^.*?@.*?:(.*)")
				user_regex = re.compile("^(.*?)@.*?:.*")
				curr_dir = curr_dir_regex.search(vte_window_title).group(1).strip()
				curr_user = user_regex.search(vte_window_title).group(1).strip()
				if curr_dir in "~":
					curr_dir = os.environ['HOME']
					if "root" in curr_user:
						curr_dir = "/root"
				if "/" in curr_dir:
					print("curr_dir:", curr_dir)
					self.tab_controller.set_active_tab_uri(curr_dir)

	def on_vte_child_exited(self, terminal, exit_code):
		print("on_vte_child_exited:", terminal)
		self.tab_controller.close_active_tab()

	def on_button_theme_clicked(self, button):
		print("on_button_theme_clicked:", button)
		print("button_name:", button.get_label())
		self.app.theme.set_theme(button.get_label())

	def on_copy_clipboard(self, *args):
		print("Copying clipboard")
		self.tab_controller.active_tab.active_term.copy_clipboard()
	
	def on_paste_clipboard(self, *args):
		print("Pasting clipboard")
		self.tab_controller.active_tab.active_term.paste_clipboard()

	def on_button_zoom_plus_clicked(self, *args):
		print("Zoom +")
		self.app.terminal_font_scale += 0.1
		self.tab_controller.active_tab.active_term.set_font_scale(self.app.terminal_font_scale)
			
	def on_button_zoom_minus_clicked(self, *args):
		print("Zoom -")
		self.app.terminal_font_scale -= 0.1
		self.tab_controller.active_tab.active_term.set_font_scale(self.app.terminal_font_scale)

	def on_terminal_focus_in_event(self, terminal, *args):
		print("Terminal focus in:", terminal)
#		font_desc.set_family("RobotoMono-Regular")
#		font_desc.set_size(12)
#		terminal.set_font(font_desc)
		self.tab_controller.active_tab.active_term = terminal












