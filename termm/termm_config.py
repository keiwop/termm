from gi.repository import Vte
import os

app_path = os.path.dirname(os.path.realpath(__file__))

app_title = "termm"
app_subtitle = "Version 0.42.2"

config_file = "termm_config.py"

ui_main_window = app_path + "/ui/termm.glade"
ui_popover_menu = app_path + "/ui/popover_menu.ui"

icons_dir = app_path + "/icons"
app_icon = icons_dir + "/termm_home.svg"

themes_dir = app_path + "/themes"
scss_dir = themes_dir + "/scss"
css_dir = themes_dir + "/css"

terminal_shell = "/usr/bin/zsh"
terminal_scrollback = 32768
terminal_encoding = "UTF-8"
terminal_scroll_output = False
terminal_scroll_key = True
terminal_default_path = os.environ['HOME']
terminal_print_working_dir = True
terminal_print_working_dir_path = False
terminal_open_tab_working_dir = True
terminal_deleted_activate_prev = True
terminal_mouse_autohide = True
terminal_bg_image = ""
terminal_font_scale = 1.0
# Cursor shape can be IBEAM, BLOCK or UNDERLINE
terminal_cursor_shape = Vte.CursorShape.UNDERLINE
terminal_default_name = "Terminal"

shortcut_box_button_min_width = 20
shortcut_box_button_min_height = 20
shortcut_box_button_hexpand = False

stack_switcher_button_min_width = 42
stack_switcher_button_min_height = 20
stack_switcher_button_hexpand = True
stack_switcher_button_drag_coef = 0.75




application_state = "drop_down"
window_states = ["window", "drop_down"]
default_screen_border = "right"
restore_on_focused_screen = True
follow_mouse = False
iconify_window = True
iconify_drop = False
hide_on_startup = False

#FIXME
margin_x = 0
margin_y = 44
window_width = 800
window_height = 480
window_x = 100
window_y = 0
coef_drop_width = 1
coef_drop_height = 0.5

window_opacity = 0.9

count_touchpad_scroll = 5

drop_down_animation = True
#Duration of the animation in seconds. Precision is at about 1ms
max_animation_time = 0.200
#The less the animation steps, the more fluid will be the animation
#For animation_step = 1 the animation might not be finished in less than 200ms
animation_step = 12


is_night_mode = False
theme_name = "light"

theme_list = {"none", "debug", "light", "dark", "solarized_light", "solarized_dark"}


terminal_palette_light = [	"#424242", "#f44336", "#2e7d32", "#ffc107", "#2962ff", "#e91e63", "#009688", "#bdbdbd", 
							"#424242", "#f44336", "#2e7d32", "#ffc107", "#2962ff", "#e91e63", "#009688", "#bdbdbd"]

terminal_palette_dark = [	"#424242", "#f44336", "#2e7d32", "#ffc107", "#2962ff", "#e91e63", "#009688", "#bdbdbd", 
							"#424242", "#f44336", "#2e7d32", "#ffc107", "#2962ff", "#e91e63", "#009688", "#bdbdbd"]

terminal_palette_solarized_light = ["#657b83", "#dc322f", "#859900", "#b58900", "#268bd2", "#d33682", "#2aa198", "#839496", 
									"#657b83", "#dc322f", "#859900", "#b58900", "#268bd2", "#d33682", "#2aa198", "#839496"]

terminal_palette_solarized_dark = [	"#586e75", "#dc322f", "#859900", "#b58900", "#268bd2", "#d33682", "#2aa198", "#eee8d5", 
									"#586e75", "#dc322f", "#859900", "#b58900", "#268bd2", "#d33682", "#2aa198", "#eee8d5"]


terminal_themes = {	"light": { 	"icons_color": "#f1f8e9", 
								"terminal_fg": "#424242", 
								"terminal_bg": "#f1f8e9", 
								"palette": terminal_palette_light},
								
					"dark": { 	"icons_color": "#424242", 
								"terminal_fg": "#f1f8e9", 
								"terminal_bg": "#424242", 
								"palette": terminal_palette_dark},
								
			"solarized_light": {"icons_color": "#2aa198", 
								"terminal_fg": "#657b83", 
								"terminal_bg": "#fdf6e3", 
								"palette": terminal_palette_solarized_light},

			"solarized_dark": { "icons_color": "#b58900", 
								"terminal_fg": "#839496", 
								"terminal_bg": "#002b36", 
								"palette": terminal_palette_solarized_dark}}



#hide_app_key = "Menu"
hide_app_key = "Control_R"
#hide_app_key = "Scroll_Lock"

shortcut_dict = {	"on_button_new_tab_clicked": 		["<Alt>Up", "<Ctrl>T"],
 					"on_button_toggle_mode_clicked": 	["<Alt>Down"],
 					"on_button_prev_tab_clicked": 		["<Alt>Left"],
 					"on_button_next_tab_clicked": 		["<Alt>Right"],
 					"on_button_fullscreen_clicked": 	["F11"],
 					"on_button_hsplit_tab_clicked": 	["<Alt>KP_0", "<Alt>KP_Insert", "<Alt>S"], 						"on_button_vsplit_tab_clicked": 	["<Alt>KP_2", "<Alt><Shift>S"],
 					"on_button_zoom_plus_clicked":		["<Ctrl>equal", "<Ctrl>plus", "<Ctrl>KP_Add"],
 					"on_button_zoom_minus_clicked":		["<Ctrl>parenright", "<Ctrl>minus", "<Ctrl>KP_Subtract"],
 					"on_copy_clipboard": 				["<Ctrl><Shift>C"],
 					"on_paste_clipboard": 				["<Ctrl><Shift>V", "<Ctrl>V"]}


button_dict = { "button_new_tab" 		: "tab-new-symbolic",
				"button_close_tab" 		: "edit-delete-symbolic",
				"button_prev_tab"		: "go-previous-symbolic",
				"button_next_tab"		: "go-next-symbolic",
				"button_hsplit_tab" 	: "object-flip-horizontal-symbolic",
				"button_vsplit_tab" 	: "object-flip-vertical-symbolic",
				"button_toggle_mode" 	: "go-top-symbolic",
				"button_zoom_plus"	 	: "zoom-in-symbolic",
				"button_zoom_minus"		: "zoom-out-symbolic",
				"button_fullscreen" 	: "view-fullscreen-symbolic",
				"button_night_mode" 	: "object-inverse",
				"header_menu" 			: "open-menu-symbolic",
				"tab_menu" 				: "open-menu-symbolic",
				"preferences" 			: "system-run-symbolic",
				"go_home" 				: "go-home-symbolic",
				"exit_app" 				: "window-close-symbolic"}




