from distutils.core import setup

setup(
	name = "termm",
	version = "0.4.2",
	packages = ["termm"],
	package_dir = {"termm": "termm"},
	package_data = {"termm": ["icons/*", "icons/buttons/*", "icons/assets/*", "ui/*", "themes/*", "themes/scss/*", "themes/css/*", "themes/scss/include/*"]},
	url = "https://github.com/keiwop/termm",
	license = "LGPLv3+",
	author = "keiwop",
	author_email = "keiwop.dev@gmail.com",
	description = "termm is a gtk3 terminal",
	requires = ["gi"]
)
