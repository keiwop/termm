#! /bin/sh

rm -rf build
mkdir -p build

cp -av MANIFEST.in build/
cp -av PKGBUILD build/
cp -av setup.py build/
cp -av termm.desktop build/
cp -av termm.sh build/
cp -av termm build/

cd build
makepkg
